Apitome::Engine.routes.draw do
  root to: "docs#index"
  get "/simulate/*path", to: "docs#simulate", as: :simulated
  get "/route/*path", to: "docs#route" unless Apitome.configuration.single_page 
  get "/*path", to: "docs#show" unless Apitome.configuration.single_page
  post 'docs/make_request/', to: "docs#make_request"
end
