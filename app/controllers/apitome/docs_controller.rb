require 'open-uri'
require 'pry-byebug'

class Apitome::DocsController < Object.const_get(Apitome.configuration.parent_controller)
  layout Apitome.configuration.layout

  helper_method *[
    :resources,
    :endpoints_tree,
    :example,
    :formatted_body,
    :param_headers,
    :param_extras,
    :formatted_readme,
    :set_example,
    :id_for,
    :rendered_markdown
  ]

  def index
  end

  def show
  end

  def route
    render 'resource'
  end

  def make_request
    request_params = request.params['request_parameters'].split
    request_query = {}
    request_params.each do |p|
      request_query[p] = request.params[p]
    end

    uri = URI::HTTP.build(host: "0.0.0.0", port: 7778)
    res = Net::HTTP.post_form(uri, request_params)
    render json: {status: res.code, body: res.body}
  end


  def simulate
    request = example["requests"][0]
    if request
      request["response_headers"].delete('Content-Length')
      request["response_headers"].each { |k, v| headers[k] = v }
      render body: request["response_body"], status: request["response_status"]
    else
      render body: "No simulation for this endpoint", status: 404
    end
  end

  private

  def file_for(file, readme: false)
    if Apitome.configuration.remote_docs
      file = if readme
        "#{file}"
      else
        "#{Apitome.configuration.doc_path}/#{file}"
      end

      file = "#{Apitome.configuration.remote_url}/#{file}"
    else
      file = Apitome.configuration.root.join(Apitome.configuration.doc_path, file)
      raise Apitome::FileNotFoundError.new("Unable to find #{file}") unless File.exists?(file)
    end

    open(file).read
  end

  def resources
    @resources ||= JSON.parse(file_for("index.json"))["resources"]
  end

  def endpoints_tree
    @endpoint_tree ||= Apitome::PathConverter.new(file_for("index.json"))
    @endpoint_tree.convert
    @endpoint_tree.root_node
    endpoints = @endpoint_tree.root_node.endpoints
    routes = endpoints.map { |e| e.data[:route] }
    out_hash = {}
    routes.each do |route|
      out_hash[route] = endpoints.select { |e| e.data[:route] == route }
    end
    out_hash
  end

  def example
    @example ||= JSON.parse(file_for("#{params[:path]}.json"))
  end

  def set_example(resource)
    @example = JSON.parse(file_for("#{resource}.json"))
  end

  def formatted_readme
    return unless Apitome.configuration.readme
    rendered_markdown(file_for(Apitome.configuration.readme, readme: true))
  end

  def rendered_markdown(string)
    if defined?(GitHub::Markdown)
      GitHub::Markdown.render(string)
    else
      Kramdown::Document.new(string).to_html
    end
  end

  def formatted_body(body, type)
    if type =~ /json/ && body.present?
      JSON.pretty_generate(JSON.parse(body))
    else
      body
    end
  rescue JSON::ParserError
    return body if body == " "
    raise JSON::ParserError
  end

  def param_headers(params)
    titles = %w{Name Description}
    titles += param_extras(params)
    titles
  end

  def param_extras(params)
    params.map do |param|
      param.reject { |k, _v| %w{name description required scope}.include?(k) }.keys
    end.flatten.uniq
  end

  def id_for(str)
    Apitome.configuration.url_formatter.call(str)
  end
end
