require 'spec_helper'
require 'pry-byebug'

describe Apitome::PathConverter do
  describe '#convert' do
    it 'converts JSON files no tree of path nodes' do
      resources = file_fixture("resources.txt").read
      converter = Apitome::PathConverter.new(resources)

      expect(converter.convert).to be_a_kind_of Array
    end
  end
end


describe Apitome::PathNode do
  let(:converter) { Apitome::PathConverter.new(file_fixture("resources.txt").read) }
  #TODO Fix this bullshit
  let(:example) { converter.resources.first.examples.first }
  let(:root_node) { converter.root_node }

  describe '#add_child' do
    context 'if route to the child is empty (child is an example)' do
      before { example[:route] = '' }
      let(:node) { root_node.add_child([], example) }
      
      it 'creates a node' do
        expect(node).to be_a_kind_of Apitome::PathNode
      end

      it 'assigns root to node\'s parent' do
        expect(node.parent).to eq root_node
      end

      it 'assigns node to root\'s children' do
        expect(root_node.children).to include(node)
      end
    end

    context 'if route to the child is not empty' do
      before do
        root_node.add_child(['v1', 'node'], example) 
      end

      it 'adds additional nodes between child and root' do
        expect(root_node.children.map(&:name)).to include 'v1'
      end

      context 'if root node already have nodes from route' do
        it 'doesn\'t add new nodes and use existing nodes'
      end
    end
  end

  describe '#child_by_name' do
    let(:result) { root_node.child_by_name(example[:description]) }

    context 'if there is such node' do
      it 'returns that node' do
        node = root_node.add_child([], example)
        expect(result).to eq node
      end
    end

    context 'if there ara no such node' do
      it 'returns nil'
    end
  end
end
