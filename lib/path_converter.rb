require 'pry-byebug'

module Apitome
  class PathConverter
    attr_reader :root_node, :resources

    def initialize(resources)
      @resources = JSON.parse(resources, symbolize_names: true)[:resources].map { |r| Resource.new(r) }
      @root_node = PathNode.new("Courier", nil)
    end

    def convert
      @resources.each do |resource|
        resource.examples.each do |example|
          @root_node.add_child(splitted_route_from(example[:route]), example)
        end
      end
    end

    private

    def splitted_route_from(route)
      route.split('/')
    end
  end

  class PathNode
    attr_reader :name, :data
    attr_accessor :parent, :children
    def initialize(name, data = nil)
      @data = data
      @name = name
      @parent = parent
      @children = []
    end

    # maybe i need to rename this method to #add_example 
    # and add additional method #add_child
    def add_child(route, data)
      if route.empty? 
        register_child(Example.new(data))
      else
        child_node = register_child(PathNode.new(route.shift))
        child_node.add_child(route, data)
      end
    end

    def child_by_name(name)
      children.find { |c| c.name == name }
    end

    def endpoints
      endpoints = []
      find_endpoints(self, endpoints)
      endpoints
    end

    private

    def find_endpoints(node, endpoints)
      if node.children.empty?
        endpoints << node 
      else
        node.children.each do |child|
          find_endpoints(child, endpoints)
        end
      end
    end

    def register_child(node)
      node.parent = self
      self.children << node
      node
    end
  end

  class Resource
    attr_reader :data

    def initialize(data)
      @data = data
    end

    def examples
      @examples ||= data[:examples]
    end
  end

  class Example < PathNode
    attr_accessor :splitted_route, :parent
    attr_reader :name

    def initialize(data, parent = nil)
      @data = data
      # TODO need to move @splitted_route somewhere couse now it is an endpoint node, it should be created from data, not from another example
      # or i need class EndNode (or EndPoint)
      @splitted_route = data[:route].split('/')
      @name = data[:description]
      # I should pass nil here instead empty array
      @children = []
    end
  end
end
